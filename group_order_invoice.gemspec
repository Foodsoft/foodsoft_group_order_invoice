require_relative "lib/group_order_invoice/version"

Gem::Specification.new do |spec|
  spec.name        = "foodsoft_group_order_invoice"
  spec.version     = GroupOrderInvoice::VERSION
  spec.authors     = [""]
  spec.email       = [""]
  spec.homepage    = "https://git.local-it.org/Foodsoft/foodsoft_group_order_invoice"
  spec.summary     = ""
  spec.description = ""

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://git.local-it.org/Foodsoft/foodsoft_group_order_invoice"
  spec.metadata["changelog_uri"] = "https://git.local-it.org/Foodsoft/foodsoft_group_order_invoice"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib,spec}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "rails", ">= 7.0.4"
end
